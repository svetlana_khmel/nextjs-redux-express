import { INPUT_VALUE } from '../constants'
import data from '../api/data'
import * as types from '../actionTypes'

import fetch from 'isomorphic-fetch';
const getMessageUrl = 'http://localhost:3200/api/messages';
//const loginUrl = '/auth/login';
//const loginUrl = window.location.origin + '/authenticate';
const loginUrl =  'http://localhost:3200/authenticate';


export const inputChange = (title, name, val) => dispatch => {

  console.log(".....inputChange  ", title, name, val);
  return dispatch({type: INPUT_VALUE, title, name, val})
};

export const getDataCreator = (data) => {
    console.log('____DATA ACTION____ ', data);
    return {
        type: types.GET_MESSAGES,
        payload: data
    }
};

export const addArticleCreator = (data) => {
    debugger;
    return {
        type: types.ADD_ARTICLE,
        payload: data
    }
};

export const addFormValue = (data) => {
    debugger;
    return {
        type: 'ADD_FORM_VALUE',
        payload: data
    }
};

export const removeArticle = (id) => {
    return {
        type: types.REMOVE_ARTICLE,
        id
    }
};

export const editArticleCreator = (data) => {
    return {
        type: types.EDIT_ARTICLE,
        data
    }
};

export const setUser = (data) => {
    return {
        type: types.LOGIN_USER,
        payload: data
    }
};

export const logOutCreator = () => {
    return {
        type: types.LOGOUT
    }
};

export const createUser = (data) => {
    return {
        type: types.REGISTER_USER,
        payload: data
    }
};

export const loadData = (token) => {
    return (dispatch, getState) => {
        return fetch(getMessageUrl)
            .then( r => r.json() )
            .then( data => {
                console.log('REQUEST HAS BEEN MADE and got data   ' ,data);
                dispatch(getDataCreator(data));
            })
            .catch((err) => console.log(err))
    }
};


// export const loadData = (token) => {
//     return (dispatch, getState) => {
//         return data.getMessages(token)
//             // .then((response) => response.json())
//     .then((data) => {
//         console.log('DATA HAS BEEN LOADED ', data);
//         dispatch(getDataCreator(data));
//     })
//     .catch((err) => console.log(err))
//     }
// };

export const loginUser = (userdata) => {
    return (dispatch) => {

        console.log('userdata  ', userdata);

        return fetch(loginUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(userdata)
        })
        .then((response) => response.json())
        .then((data) => {
            console.log('ddd', data);
            dispatch(setUser(data))
        })
        .catch((err) => console.log(err))
    }
};

// export const loginUser = (userdata) => {
//     return (dispatch) => {
//         return data.loginUser(userdata)
//             .then((response) => response.json())
//     .then((data) => {
//             console.log('ddd', data)
//         dispatch(setUser(data))
//     })
//     .catch((err) => console.log(err))
//     }
// };

export const registerUser = (userdata) => {
    return (dispatch) => {
        return data.registerUser(userdata)
            .then((response) => response.json())
    .then((data) => dispatch(createUser(data)))
    .catch((err) => console.log(err))
    }
};

export const deleteArticle = (id) => {
    return (dispatch) => {
        return data.deleteArticle(id)
            .then((response) => response.json())
    .then((data) => dispatch(removeArticle(id)))
    .catch((err) => console.log(err))
    }
};

export const editArticle = (token, id, article) => {
    return (dispatch) => {
        return data.editArticle(token, id, article)
            .then((response) => response.json())
    .then((data) => dispatch(editArticleCreator(data)))
    .catch((err) => console.log(err))
    }
};


export const addArticle = (article) => {
    return (dispatch) => {
        return fetch('/api/message', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(article)
        }).then( data => {
           dispatch(addArticleCreator(data));
        });

        return data.addArticle(article)
            .then((response) => response.json())
            .then((data) => dispatch(addArticleCreator(data)))
            .catch((err) => console.log(err))
    }
};

// export const addArticle = (token, id, article) => {
//     return (dispatch) => {
//         return data.addArticle(token, id, article)
//             .then((response) => response.json())
//     .then((data) => dispatch(addArticleCreator(data)))
//     .catch((err) => console.log(err))
//     }
// };
//


