const express = require('express');
const next = require('next');

const dev = process.env.NODE_ENV !== 'production';
const app = next({dev});
const handle = app.getRequestHandler();
const port = parseInt(process.env.PORT, 10) || 3200;

const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

var Message = require('./model/message');
var User = require('./model/user');

const { parse } = require('url');
const mongoose = require('mongoose');
const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://localhost:27017/test';

app.prepare()
    .then(() => {
        const server = express();
        server.use(bodyParser.urlencoded({extended: false}));
        // Parse application/json
        server.use(bodyParser.json());

        // Allows for cross origin domain request:
        server.use(function (req, res, next) {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
            next()
        });

        // Request body parsing middleware should be above methodOverride

        server.use(cookieParser());

        // MongoDB
        mongoose.connect(MONGODB_URI, {useMongoClient: true});

        // Verify username and password, if passed, we return jwt token for client
        // We also include xsrfToken for client, which will be used to prevent CSRF attack
        // and, you should use random complicated key (JWT Secret) to make brute forcing token very hard
        server.post('/authenticate', (req, res) => {
            const { username, password } = req.body;

            console.log('_____________', username);

            console.log("USER WAS FOUND::   ", req.body);

            User.findOne({username: req.body.username}, function (err, user) {
                if (err) throw err;

                if (!user) {

                    res.json({ success: false, message: 'Authentication failed. User not found.' });

                } else if (user) {
                    //check if password matches
                    if (user.password != req.body.password) {
                       // res.json({ success: false, message: 'Authentication failed. Wrong password.' });


                        res.status(400).json({
                            success: false,
                            message: 'Authentication failed'
                        })

                    } else {
                        // if user is found and password is right

                            var token = jwt.sign({
                                username: username,
                                xsrfToken: crypto.createHash('md5').update(username).digest('hex')
                            }, 'jwtSecret', {
                                expiresIn: 60*60
                            });

                            res.status(200).json({
                                success: true,
                                message: 'Enjoy your token',
                                token: token
                            })

                        // var token = createToken (user);
                        //
                        // // return the information including token as JSON
                        // res.json({
                        //     success: true,
                        //     message: 'Enjoy your token!',
                        //     token: token,
                        //     user: user.username,
                        // });
                    }
                }

                console.log("USER WAS FOUND::   ", user);
            })

            // if (username === 'test' || password === 'test') {
            //     var token = jwt.sign({
            //         username: username,
            //         xsrfToken: crypto.createHash('md5').update(username).digest('hex')
            //     }, 'jwtSecret', {
            //         expiresIn: 60*60
            //     });
            //     res.status(200).json({
            //         success: true,
            //         message: 'Enjoy your token',
            //         token: token
            //     })
            // } else {
            //     res.status(400).json({
            //         success: false,
            //         message: 'Authentication failed'
            //     })
            // }
        });

        // Authenticate middleware
        // We will apply this middleware to every route except '/login' and '/_next'
        server.use(unless(['/login', '/_next'], (req, res, next) => {
            const token = req.cookies['x-access-token'];
            if (token) {
                jwt.verify(token, 'jwtSecret', (err, decoded) => {
                    if (err) {
                        res.redirect('/login');
                    } else {
                        // if everything is good, save to request for use in other routes
                        req.decoded = decoded;
                        next();
                    }
                })
            } else {
                res.redirect('/login');
            }
        }));

        // Api example to prevent CRSF attack
        server.post('/api/preventCRSF', (req, res, next) => {
            if (req.decoded.xsrfToken === req.get('X-XSRF-TOKEN')) {
                res.status(200).json({
                    success: true,
                    message: 'Yes, this api is protected by CRSF attack'
                })
            } else {
                res.status(400).json({
                    success: false,
                    message: 'CRSF attack is useless'
                })
            }
        });

        server.get('/', (req, res) => {
            return handle(req, res)
        });

        server.get('*', (req, res) => {
            const parsedUrl = parse(req.url, true);
            const { pathname, query } = parsedUrl;

            if (pathname === '/api/messages') {
                app.render(req, res, '/b', query);

                Message.find({}, function (err, result) {
                    if (err) return next(err);

                    console.log('RESULT:  ', result);

                    res.json(result);
                })


            } else if (pathname === '/b') {
                app.render(req, res, '/a', query)
            } else {
                handle(req, res, parsedUrl)
            }

            //return handle(req, res)
        });

        server.get('/contact', (req, res) => {
            return handle(req, res)
        });

        server.get('/new', (req, res) => {
            return handle(req, res)
        });

        server.get('/about', (req, res) => {
            return handle(req, res)
        });

        server.post('/api/message', (req, res) => {
            console.log("Message_ _++++++ _ ______/api/message", req.body);
            req.body.user = req.user;

            var message = new Message({data: req.body});

            console.log(". . . .. . . . . ", message);

            message.save(function (err, product, numAffected) {
                // console.log(". . . .. . . . . ", message);
                res.json(message);
            });
        });

        server.listen(port, (err) => {
            if (err) throw err;
            console.log('> Ready on http://localhost:' + port);
        })
    })
    .catch((ex) => {
        console.error(ex.stack);
        process.exit(1)
    });

function unless (paths, middleware) {
    return function(req, res, next) {
        let isHave = false
        paths.forEach((path) => {
            if (path === req.path || req.path.includes(path)) {
                isHave = true
                return
            }
        })
        if (isHave) {
            return next()
        } else {
            return middleware(req, res, next)
        }
    }
}
