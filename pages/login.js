import React, { Component } from 'react'
import withRedux from 'next-redux-wrapper'

import Login from '../components/login'

import { initStore } from '../store'

class LoginPage extends Component {
    render () {
        return <Login />
    }
}

export default withRedux(initStore, null)(LoginPage);