import React, { Component } from 'react'
import withRedux from 'next-redux-wrapper'

import New from '../components/newPostIndex.js'

import { initStore } from '../store'

class Index extends Component {
    render () {
        return <New />
    }
}

export default withRedux(initStore, null)(Index)