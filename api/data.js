import fetch from 'isomorphic-fetch';

// const loginUrl = '/auth/login';
// const registerUrl = '/auth/register';
const getMessageUrl = 'http://localhost:3200/api/messages';
// const editArticleUrl = '/auth/editArticle';
// const deleteArticleUrl = '/auth/deleteArticle';
// const addArticleUrl = '/api/message';
//

// const data = {
//     getMessages () {
//         console.log( "Data has been tacken ,  ",  fetch('http://localhost:3200/api/messages'));
//
//         return fetch('http://localhost:3200/api/messages')
//     }
// }
//
//
// const data = {
//     getMessages () {
//          return fetch(dataUrl)
//     }
// }


// import fetch from 'isomorphic-fetch'
//
// const dataUrl = 'http://localhost:5000/api/messages';
// // const dataUrl = '/api';
//
// export function getPosts () {
//     return fetch(dataUrl)
// }

 const data = {
     getMessages() {
         console.log('REQUEST HAS BEEN MADE');

         fetch(getMessageUrl)
             .then( r => r.json() )
             .then( data => {
                console.log('REQUEST HAS BEEN MADE and got data   ' ,data);

                 return data;
             });

         // return fetch(getMessageUrl, {
         //    method: 'POST',
         //    headers: {
         //        'Accept': 'application/json',
         //        'Content-Type': 'application/json',
         //        //'Content-Type': 'text/html',
         //    },
         //    body: JSON.stringify({
         //       // token
         //    })
       // });
     },

    // getMessages (token) {
    //     let lyricsData;
    //
    //     if (navigator.onLine === false ) {
    //         if(typeof Storage !== 'undefined' && localStorage.getItem('lyricsData').length !== 0) {
    //             let promise = new Promise((resolve, reject) =>{
    //                 resolve(lyricsData)
    //             })
    //         }
    //     }
    //
    //     return fetch(getMessageUrl, {
    //         method: 'POST',
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json',
    //             //'Content-Type': 'text/html',
    //         },
    //         body: JSON.stringify({
    //             token
    //         })
    //     });
    // },

    loginUser (data) {
        return fetch(loginUrl, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                //'Content-Type': 'text/html'
            },
            body: JSON.stringify(
                data
            )
        });
    },

    registerUser (data) {
        return fetch(registerUrl, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                data
            })
        })
    },

    addArticle(token, data) {
        return fetch(addArticleUrl, {
            method: 'post',
            headers: {
                'Accept': 'application.json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                data,
                token
            })
        });
    },

    editArticle(token, id, data) {
        return fetch(`${editArticleUrl}/${id}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application.json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                data,
                token
            })
        });
    },

    deleteArticle(id) {
        return fetch(`${deleteArticleUrl}/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application.json',
                'Content-Type': 'application/json'
            }
        });
    }
};

export default data;
