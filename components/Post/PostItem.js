import React from 'react'
import { Link } from 'routes'

const PostItem = ({ post }) => (
<div>
<div className={'article'} key={post.data.id}>
<div className={'title'} key={post.data.id}>{post.data.title}</div>
<div className={'post'}>{post.data.article[0]}</div>
<div className={'translation'}>{post.data.translation[0]}</div>
<div className={'category'}>{post.data.category}</div>
</div>
</div>
)

export default PostItem
