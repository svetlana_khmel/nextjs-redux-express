import React, {Component} from 'react'
import Head from 'next/head'
import { Col, Row } from 'react-bootstrap'
import Header from './Header'
import DisplayForm from './DisplayForm'

import UserForm from './UserForm'
import Social from './Social'
import Input from '../handlers/Input'

import Nav from 'components/Nav';
import Articles from 'components/Articles';



class Main extends Component {
  render () {
    return (
      <div>
        <Nav />
        <Head>
          <title>Form Handler</title>
          <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css' />
        </Head>
        <Header />
        <DisplayForm />
        <Row>
          <Col lg={6}>
            <UserForm />
          </Col>
          <Col lg={6}>
            <Social />
          </Col>
        </Row>
          adasdasdasdasd
          <Input controlLabel='Last name' title='user' name='lastName' />
          <Articles />
      </div>
    )
  }
}

export default Main
