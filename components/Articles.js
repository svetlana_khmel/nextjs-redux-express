import React, {Component} from 'react';
import { getPosts } from 'api/data';
import { connect } from 'react-redux'
import fetch from 'isomorphic-fetch'
//import { inputChange } from '../actions/'

import DisplayForm from './DisplayForm'

import Input from '../handlers/Input';
import {bindActionCreators} from 'redux';
import { loadData } from '../actions/';

//import withRedux from 'next-redux-wrapper'
//import { initStore } from '../store'

class Articles extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount () {
        this.props.loadData();
        //console.log(this.props);
    }

    render () {
        return  <div>
                    {/*<ul>*/}
                        {/*{posts.map(p => <PostItem key={p.data.id} post={p} />)}*/}
                    {/*</ul>*/}
                </div>;
    }
}

const mapDispatchToProps = dispatch => {
    return {
        loadData: bindActionCreators(loadData, dispatch)
    }
};

const mapStateToProps = state => {
    console.log('state .... ', state);
    return {
        state
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Articles);