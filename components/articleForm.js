import React from 'react';

const ArticleForm = ({titleValue, getInputData, handleFileUpload, articleValue, translationValue, categogyValue, sendData, inputTitleRef}) => {
    return (
        <div className={'new-article'}>
            <div className="title">
                <label>Title: (New)</label>
                <input value={titleValue} data-attr="title" onChange={getInputData} onKeyUp={getInputData} type="text" className={'text-input'} ref={inputTitleRef}/>
            </div>
            <div className="original">
                <label>original</label>
                <input data-attr="original" type="file" onChange={handleFileUpload} className={'text-input'} />
            </div>
            <div className="article-body">
                <label>Lyrics</label>
                <textarea value={articleValue} data-attr="article" onChange={getInputData} onKeyUp={getInputData} className={'textarea'} rows="15"></textarea>
            </div>
            <div className="translation-body">
                <label>Translation</label>
                <textarea value={translationValue} data-attr="translation" onChange={getInputData} onKeyUp={getInputData} className={'textarea'} rows="15"></textarea>
            </div>
            <div className="category-body">
                <label>Category</label>
                <textarea value={categogyValue} data-attr="category" onChange={getInputData} onKeyUp={getInputData} className={'textarea'}></textarea>
            </div>

            <div className="minus">
                <label>minus</label>
                <input data-attr="minus" type="file" onChange={handleFileUpload} />
            </div>

            <button className="btn send" onClick={sendData}>Send</button>
        </div>
    )
};

export default ArticleForm;