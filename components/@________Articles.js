import React from 'react';
// import Layout from 'layouts/Main'
import { getPosts } from 'api/data';
import PostItem from 'components/Post/PostItem';
import Nav from 'components/Nav';
import Inputtest from './inputtest';


import withRedux from 'next-redux-wrapper'
import { initStore } from '../store'

const IndexPage = ({ posts }) =>
    <div>
        <Nav />

        <Inputtest />

        <button onClick={() => {
            console.log('.....AVAVHA!!! ');
        }}>Load</button>

        <ul>
            {posts.map(p => <PostItem key={p.data.id} post={p} />)}
        </ul>
    </div>;


IndexPage.getInitialProps = async ({ req }) => {
    const res = await getPosts();
    const json = await res.json();

    // console.log('*******    ', json);
    return { posts: json }
};

export default withRedux(initStore, null)(IndexPage)