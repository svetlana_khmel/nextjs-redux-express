import React, {Component} from 'react';
import {loginUser} from '../actions';
import {connect} from 'react-redux';

import Router from 'next/router'

import md5 from 'md5';

import { setCookie } from '../utils/CookieUtils'


class Login extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentWillReceiveProps (nextProps) {
        const {user} = nextProps.state;

        if (user.success) {
            debugger;
            setCookie('x-access-token', user.token);
            Router.push({
                pathname: '/'
            })
        }
    }

    componentDidMount () {
        const {user} = this.props.state;

        if (user.success) {
            this.props.url.replaceTo('/')   // redirect if you're already logged in
        }
    }

    handleSubmit (e) {
        e.preventDefault();

        const username = this.refs.email.value;
        const password = this.refs.password.value;

        const data = {
            username,
            password: md5(password)
        };

        this.props.loginUser(data);
    }

    render () {
        return (
            <div>
                Login
                <form onSubmit={this.handleSubmit} >
                    <input type="text" ref="email"/>
                    <input type="password" ref="password"/>
                    <input type="submit" value="Submit"/>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        state
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        loginUser: (data) => {
            dispatch(loginUser(data));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

