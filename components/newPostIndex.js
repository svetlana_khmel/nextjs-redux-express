import React, {Component} from 'react';
import DisplayForm from './DisplayForm'
import Input from '../handlers/Input'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {addArticle} from '../actions';

class newPost extends Component {
    constructor(props) {
        super();
    }

    submitData = () => {
        console.log("Props", this.props);
        console.log("State", this.props.state.formReducer);

        const title = this.props.state.formReducer.Title.title || '';
        const lyrics = this.props.state.formReducer.Lyrics.lyrics || '';
        const translation = this.props.state.formReducer.Translation.translation || '';
        const category = this.props.state.formReducer.Category.category || '';


        const data = {
            title: /\n/.test(title) ? title.split(/\n/): title || '',
            article: /\n/.test(lyrics) ? lyrics.split(/\n/): lyrics || '',
            translation: /\n/.test(translation) ? translation.split(/\n/): translation || '',
            category: /\n/.test(category) ? category.split(/\n/): category || ''
        };

        this.props.addArticle(data);
    };

    render () {
        return <div>
            <h1>NEW POST:</h1>

            <DisplayForm />

            <Input controlLabel='Title' title='Title' name='title' />
            <Input controlLabel='Lyrics' title='Lyrics' name='lyrics' componentClass='textarea' />
            <Input controlLabel='Translation' title='Translation' name='translation' componentClass='textarea' />
            <Input controlLabel='Category' title='Category' name='category' />

            <button id="something-btn" type="button" class="btn btn-success btn-sm" onClick={this.submitData}>
                Something
            </button>
        </div>
    }
}

//export default newPost;
//
// const mapDispatchToProps = dispatch => {
//     return {
//         loadData: bindActionCreators(loadData, dispatch)
//     }
// };
//
// const mapStateToProps = state => {
//
//     console.log('state .... ', state);
//     return {
//         state
//     }
// };

//export default connect(mapStateToProps, mapDispatchToProps)(Articles);

const mapStateToProps = state => {
    return {
        state
    }
};

const mapDispatchToProps = dispatch => {
    return {
        addArticle: bindActionCreators(addArticle, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(newPost);
