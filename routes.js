const nextRoutes = require('next-routes');
const routes = module.exports = nextRoutes();

routes.add('index', '/');
routes.add('about', '/about');
routes.add('api', '/api');
routes.add('contact', '/contact');

routes.add('new', '/new');
routes.add('post', '/blog/:slug');
