import { combineReducers } from 'redux';
import formReducer from './formReducer';
import data from './dataReducer';
import user from './loginUserReducer';

export default combineReducers({
  formReducer,
  data,
  user
});
