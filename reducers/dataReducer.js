import * as types from '../actionTypes';

let initialState = [];

export default (state = initialState, action) => {
    switch (action.type) {
        case types.GET_MESSAGES:

            console.log('____DATA REDUCER:  ', [...state, ...action.payload]);

            return [...state, ...action.payload];

        case types.ADD_ARTICLE:
            return [...state, action.payload];

        case types.REMOVE_ARTICLE:
            return state.filter((article) => {

                if (article._id !== action.id) {
                    return article;
                }
            });

        case types.EDIT_ARTICLE:
            return state.map( (article, index) => {
                if(article._id !== action.data._id) {
                    // This isn't the item we care about - keep it as-is
                    return article;
                }
                // Otherwise, this is the one we want - return an updated value

                console.log("...edit ..", {...article, ...action.data});
                console.log("...state ..", ...state);

                return {
                    ...article,
                    ...action.data
                };
            });

        default:
            return state
    }
}