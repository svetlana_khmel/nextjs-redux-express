# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

https://medium.com/@paul_irish/debugging-node-js-nightlies-with-chrome-devtools-7c4a1b95ae27

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Handfull articles ###

* Debugger in node.js

* https://medium.com/@paul_irish/debugging-node-js-nightlies-with-chrome-devtools-7c4a1b95ae27

* https://habrahabr.ru/post/323588/
* https://medium.com/styled-components/building-a-blog-with-next-js-359cf1236574

* https://staart.nmr.io/

https://medium.com/@positivecarlos/authentication-on-universal-react-with-next-js-b441ef458046
https://medium.com/@positivecarlos/authentication-on-universal-react-with-next-js-b441ef458046

# Good example: #

* https://github.com/timberio/next-go

# MongoBD and next.js #

* http://thecodebarbarian.com/building-a-nextjs-app-with-mongodb.html?utm_source=hashnode.com

* https://github.com/vkarpov15/thecodebarbarian.com/blob/master/lib/posts/20170426_nextjs_mongodb.md

* https://github.com/tomsoderlund/nextjs-express-mongoose-crudify-boilerplate


# JWT Authorisation example #

* https://github.com/zeit/next.js/issues/153
https://github.com/trandainhan/next.js-example-authentication-with-jwt



# Building a Blog With Next.js and styled-components #

* https://medium.com/styled-components/building-a-blog-with-next-js-359cf1236574
* https://github.com/timberio/next-go/tree/master/components


* https://github.com/fridays/next-routes

# Use ESLint Like a Pro with ES6 and React #

* http://www.zsoltnagy.eu/use-eslint-like-a-pro-with-es6-and-react/

* Code review
* Other guidelines

### How to debug ###

* Add debugger to getInitialProps
* Setup a custom server on the backend so that you can call node server.js instead of next start (explained here)
* Run node --inspect server.js (I think you need node version >= 8.0.0)
* If in chrome, open the node debugger: go to about:inspect and click "Open dedicated DevTools for Node"
* Load the page with debugger in getInitialProps - it should pause execution for you to debug